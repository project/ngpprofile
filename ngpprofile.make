core = "6.x"

; Modules

projects[] = activitystream
projects[] = addthis
projects[] = admin
projects[] = admin_menu
projects[] = audio
projects[] = calendar
projects[] = captcha
projects[] = cck
projects[] = contemplate
projects[] = context
projects[] = crmapi
projects[] = crmapi_node
projects[] = crmngp
projects[] = ctools
projects[] = date
projects[] = devel
projects[] = draggableviews
projects[] = email
projects[] = emfield
projects[] = excerpt
projects[] = features
projects[] = filefield
projects[] = flickr
projects[] = follow
projects[] = forward
projects[] = getid3
projects[] = gmap
projects[] = google_analytics
projects[] = image
projects[] = imageapi
projects[] = imagecache
projects[] = imagecrop
projects[] = imagefield
projects[] = imagefield_crop
projects[] = imce
projects[] = imce_wysiwyg
projects[] = install_profile_api
projects[] = jquery_ui
projects[] = jquery_update
projects[] = jquery_plugin
projects[] = jstools
projects[] = letters
projects[] = lightbox2
projects[] = lineage
projects[] = link
projects[] = location
projects[] = logintoboggan
projects[] = manager
projects[] = media_youtube
projects[] = messaging
projects[] = ngpcampaign
projects[] = ngplinks
projects[] = nodewords
projects[] = notifications
projects[] = panels
projects[] = pathauto
projects[] = path_redirect
projects[] = petition
projects[] = print
projects[] = purl
projects[] = rotor
projects[] = scheduler
projects[] = service_links
projects[] = spaces
projects[] = splash
projects[] = teaserbytype
projects[] = token
projects[] = upload_element
projects[] = vertical_tabs
projects[] = views
projects[] = views_bonus
projects[] = views_bulk_operations
projects[] = webform
projects[] = wysiwyg
projects[] = xmlsitemap

; Themes

projects[] = blueprint
projects[] = ngp
projects[] = kommunity
projects[] = magazeen
projects[] = pixture
projects[] = twilight
projects[] = wabi

projects[tao][type] = theme
projects[tao][download][type] = git
projects[tao][download][url] = git://github.com/developmentseed/tao.git

projects[rubik][type] = theme
projects[rubik][download][type] = git
projects[rubik][download][url] = git://github.com/developmentseed/rubik.git

; Libraries

libraries[blueprint][download][type] = "git"
libraries[blueprint][download][url] = "git://github.com/joshuaclayton/blueprint-css.git"
libraries[blueprint][directory_name] = "blueprint"
libraries[blueprint][destination] = "themes/blueprint"

libraries[jquery_ui][download][type] = "get"
libraries[jquery_ui][download][url] = "http://jquery-ui.googlecode.com/files/jquery.ui-1.6.zip"
libraries[jquery_ui][directory_name] = "jquery.ui"
libraries[jquery_ui][destination] = "modules/jquery_ui"

libraries[getid3][download][type] = "get"
libraries[getid3][download][url] = "http://kent.dl.sourceforge.net/project/getid3/getID3%28%29%201.x/1.7.9/getid3-1.7.9.zip"

libraries[nusoap][download][type] = "get"
libraries[nusoap][download][url] = "http://voxel.dl.sourceforge.net/project/nusoap/nusoap/0.7.3/nusoap-0.7.3.zip"
libraries[nusoap][destination] = "libraries/nusoap"

libraries[tinymce][download][type] = "get"
libraries[tinymce][download][url] = "http://kent.dl.sourceforge.net/project/tinymce/TinyMCE/3.3.2/tinymce_3_3_2.zip"

