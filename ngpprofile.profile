<?php
/**
 * NGP Campaign Install Profile
 * http://www.ngpsoftware.com
 *
 * Original code borrowed from Atrium Installer by Development Seed
 * http://www.developmentseed.org
 */

/**
 * Implementation of hook_profile_details().
 */
function ngpprofile_profile_details() {
  return array(
    'name' => 'NGP Campaign',
    'description' => 'Basic campaign defaults and features provided by NGP Software.'
  );
}

/**
 * Implementation of hook_profile_modules().
 */
function ngpprofile_profile_modules() {
  $modules = array(
    // Install Profile API
    'install_profile_api',
    // Drupal core
    'aggregator', 'block', 'comment', 'dblog', 'filter', 'help', 'menu', 'node', 'openid',
    'search', 'system', 'taxonomy', 'upload', 'user',
    // Admin
    'admin', 'admin_menu', 'devel', 'logintoboggan', 'manager', 'managers_bar', 'vertical_tabs',
    // CCK
    'content', 'content_copy', 'nodereference', 'text', 'optionwidgets', 'link', 'filefield', 'imagefield', 'emfield', 'emvideo', 'number',
    // jQuery
    'jquery_plugin', 'jquery_ui',
    // Rotor
    'upload_element', 'rotor',
    // Chaos Tools
    'ctools',
    // Date
    'date_api', 'date_timezone', 'date_php4',
    // Image
    'imageapi', 'imageapi_gd', 'imagecache', 'imagecache_ui', 'imagefield_crop',
    // Location
    'location', 'location_cck',
    // Token
    'token',
    // WYSIWYG
    'wysiwyg', 'imce', 'imce_wysiwyg',
  );

  return $modules;
}

/**
 * Returns an array list of NGP features (and supporting) modules.
 */
function _ngpprofile_required_modules() {
  return array(
    // Context
    'purl', 'context', 'context_contrib', 'context_ui',
    // Features
    'features',
    // Panels
    'panels', 'page_manager',
    // Spaces
    'spaces', 'spaces_site',
    // Views
    'views', 'views_ui', 'views_content', 'views_bonus_panels', 'views_bulk_operations', 'draggableviews', 'draggableviews_cck',
  );
}

/**
 * Returns an array list of NGP features (and supporting) modules.
 */
function _ngpprofile_campaign_modules() {
  return array(
    // Core features
    'blog', 'contact', 'path', 'color',
    // Calendar, date
    'date', 'date_popup',
    // Flickr
    'flickr', 'flickr_block', 'flickr_sets',
    // NGP Campaign features
    'ngpcampaign_media', 'ngpcampaign_issues', 'ngpcampaign_video', 'ngpcampaign_events', 'ngpcampaign_home',
    // Action modules
    'follow', 'forward', 'letters', 'ngplinks', 'petition',
    // NGP API modules
    'crmapi', 'crmapi_node', 'crmngp',
    // SEO
    'google_analytics', 'nodewords', 'nodewords_basic', 'nodewords_verification_tags', 'pathauto', 'path_redirect', 'xmlsitemap', 'xmlsitemap_basic', 'xmlsitemap_node',
    // CAPTCHA
    'captcha', 'image_captcha',
  );
}

/**
 * Implementation of hook_profile_task_list().
 */
function ngpprofile_profile_task_list() {
  $tasks['required-modules-batch'] = st('Install required modules');
  $tasks['campaign-modules-batch'] = st('Install campaign modules');
  $tasks['campaign-configure-batch'] = st('Configure site');
  return $tasks;
}

/**
 * Implementation of hook_profile_tasks().
 */
function ngpprofile_profile_tasks(&$task, $url) {
  install_include(ngpprofile_profile_modules()); 
  global $profile;
  
  // Just in case some of the future tasks adds some output
  $output = '';
  
  // Redirect to module install
  if ($task == 'profile') {
    $task = 'required-modules';
  }

  // We are running a batch task for this profile so basically do nothing and return page
  if (in_array($task, array('required-modules-batch', 'campaign-modules-batch', 'campaign-configure-batch'))) {
    include_once 'includes/batch.inc';
    $output = _batch_page();
  }
  
  // Install some more modules and maybe localization helpers too
  if ($task == 'required-modules') {
    $modules = _ngpprofile_required_modules();
    $files = module_rebuild_cache();
    // Create batch
    foreach ($modules as $module) {
      $batch['operations'][] = array('_install_module_batch', array($module, $files[$module]->info['name']));
    }    
    $batch['finished'] = '_ngpprofile_modules_batch_finished';
    $batch['title'] = st('Installing @drupal', array('@drupal' => drupal_install_profile_name()));
    $batch['error_message'] = st('The installation has encountered an error.');

    // Start a batch, switch to 'required-modules-batch' task. We need to
    // set the variable here, because batch_process() redirects.
    variable_set('install_task', 'required-modules-batch');
    batch_set($batch);
    batch_process($url, $url);
    // Just for cli installs. We'll never reach here on interactive installs.
    return;
  }
  
  // Install some more modules and maybe localization helpers too
  if ($task == 'campaign-modules') {
    $modules = _ngpprofile_campaign_modules();
    $files = module_rebuild_cache();
    // Create batch
    foreach ($modules as $module) {
      $batch['operations'][] = array('_install_module_batch', array($module, $files[$module]->info['name']));
    }    
    $batch['finished'] = '_ngpprofile_profile_batch_finished';
    $batch['title'] = st('Installing @drupal', array('@drupal' => drupal_install_profile_name()));
    $batch['error_message'] = st('The installation has encountered an error.');

    // Start a batch, switch to 'intranet-modules-batch' task. We need to
    // set the variable here, because batch_process() redirects.
    variable_set('install_task', 'campaign-modules-batch');
    batch_set($batch);
    batch_process($url, $url);
    // Just for cli installs. We'll never reach here on interactive installs.
    return;
  }

  // Run additional configuration tasks
  // @todo Review all the cache/rebuild options at the end, some of them may not be needed
  // @todo Review for localization, the time zone cannot be set that way either
  if ($task == 'campaign-configure') {
    $batch['title'] = st('Configuring @drupal', array('@drupal' => drupal_install_profile_name()));
    $batch['operations'][] = array('_ngpprofile_campaign_configure', array());
    $batch['operations'][] = array('_ngpprofile_campaign_configure_variables', array());
    $batch['operations'][] = array('_ngpprofile_campaign_configure_check', array());
    $batch['finished'] = '_ngpprofile_campaign_configure_finished';
    variable_set('install_task', 'campaign-configure-batch');
    batch_set($batch);
    batch_process($url, $url);
    
    // Just for cli installs. We'll never reach here on interactive installs.
    return;
  }  

  return $output;
}

/**
 * Finished callback for the modules install batch.
 */
function _ngpprofile_modules_batch_finished($success, $results) {
  variable_set('install_task', 'campaign-modules');
}

/**
 * Finished callback for the modules install batch.
 */
function _ngpprofile_profile_batch_finished($success, $results) {
  variable_set('install_task', 'campaign-configure');
}

/**
 * Configuration. First stage.
 */
function _ngpprofile_campaign_configure() {
  // Create freetagging vocab
  $vocab = array(
    'name' => 'Keywords',
    'multiple' => 0,
    'required' => 0,
    'hierarchy' => 0,
    'relations' => 0,
    'module' => 'event',
    'weight' => 0,
    'nodes' => array('blog' => 1, 'event' => 1, 'news' => 1, 'press' => 1),
    'tags' => TRUE,
    'help' => t('Enter tags related to your post.'),
  );
  taxonomy_save_vocabulary($vocab);

  // Set time zone
  $tz_offset = date('Z');
  variable_set('date_default_timezone', $tz_offset);
  
  // Set up the action menu
  db_query("INSERT INTO {menu_custom} (menu_name, title, description) VALUES ('action', 'Action links', 'The Action link menu contains links to the various acitivty modules')");
  $links = array('epostcard', 'contribute', 'volunteer', 'letter');
  db_query("UPDATE {menu_links} SET menu_name = 'action' WHERE menu_name = 'navigation' and link_path IN (" . db_placeholders($links, 'varchar') . ")", $links);
  
  menu_rebuild();
  
  $contribute = menu_link_load(db_result(db_query("SELECT mlid FROM {menu_links} WHERE link_path = 'contribute'")));
  $contribute = array_merge((array)$contribute, array('original_item' => $contribute, 'customized' => 1, 'weight' => -50, 'menu_name' => 'action', 'plid' => '0',));
  menu_link_save($contribute);
  
  $volunteer = menu_link_load(db_result(db_query("SELECT mlid FROM {menu_links} WHERE link_path = 'volunteer'")));
  $volunteer = array_merge((array)$volunteer, array('original_item' => $volunteer, 'customized' => 1, 'link_title' => 'Get involved', 'weight' => -49, 'menu_name' => 'action', 'plid' => '0',));
  menu_link_save($volunteer);
  
  $epostcard = menu_link_load(db_result(db_query("SELECT mlid FROM {menu_links} WHERE link_path = 'epostcard'")));
  $epostcard = array_merge((array)$epostcard, array('original_item' => $epostcard, 'customized' => 1, 'link_title' => 'Tell your friends', 'weight' => -48, 'menu_name' => 'action', 'plid' => '0',));
  menu_link_save($epostcard);
  
  $letter = menu_link_load(db_result(db_query("SELECT mlid FROM {menu_links} WHERE link_path = 'letter'")));
  $letter = array_merge((array)$letter, array('original_item' => $letter, 'customized' => 1, 'link_title' => 'Write a letter', 'weight' => -47, 'menu_name' => 'action', 'plid' => '0',));
  menu_link_save($letter);
  
  // Set up the primary menu
  $home = menu_link_load(db_result(db_query("SELECT mlid FROM {menu_links} WHERE link_path = 'home'")));
  $home = array_merge((array)$home, array('original_item' => $home, 'customized' => 1, 'weight' => -50, 'expanded' => 1,));
  menu_link_save($home);
  
  $media = menu_link_load(db_result(db_query("SELECT mlid FROM {menu_links} WHERE link_path = 'media'")));
  $media = array_merge((array)$media, array('original_item' => $media, 'customized' => 1, 'weight' => -49, 'expanded' => 1,));
  menu_link_save($media);
  
  $news = menu_link_load(db_result(db_query("SELECT mlid FROM {menu_links} WHERE link_path = 'media/news'")));
  $news = array_merge((array)$news, array('original_item' => $news, 'customized' => 1, 'weight' => -48, 'plid' => $media['mlid'],));
  menu_link_save($news);
  
  $press = menu_link_load(db_result(db_query("SELECT mlid FROM {menu_links} WHERE link_path = 'media/press'")));
  $press = array_merge((array)$press, array('original_item' => $press, 'customized' => 1, 'weight' => -47, 'plid' => $media['mlid'],));
  menu_link_save($press);
  
  $issues = menu_link_load(db_result(db_query("SELECT mlid FROM {menu_links} WHERE link_path = 'issues'")));
  $issues = array_merge((array)$issues, array('original_item' => $issues, 'customized' => 1, 'weight' => -46, 'expanded' => 1,));
  menu_link_save($issues);
  
  $events = menu_link_load(db_result(db_query("SELECT mlid FROM {menu_links} WHERE link_path = 'events'")));
  $events = array_merge((array)$events, array('original_item' => $events, 'customized' => 1, 'weight' => -45, 'expanded' => 1,));
  menu_link_save($events);
  
  $contact = menu_link_load(db_result(db_query("SELECT mlid FROM {menu_links} WHERE link_path = 'contact'")));
  $contact = array_merge((array)$contact, array('original_item' => $contact, 'customized' => 1, 'weight' => 49, 'hidden' => 0, 'menu_name' => 'primary-links', 'plid' => '0', 'expanded' => 1,));
  menu_link_save($contact);
  
  // Additional contribute link
  $contribute = array('customized' => 1, 'weight' => 50, 'hidden' => 0, 'link_path' => 'contribute', 'link_title' => 'Contribute', 'menu_name' => 'primary-links', 'plid' => '0', 'expanded' => 1,);
  menu_link_save($contribute);
}

/**
 * Configuration. Second stage.
 */
function _ngpprofile_campaign_configure_variables() {
  // Set NGP Links defaults
  variable_set('ngplinks_cwp_subscribe_url', 'crmapi/subscribe');
  variable_set('ngplinks_cwp_contribute_url', 'https://'.$_SERVER['SERVER_NAME'].'/crmapi/contribute');
  variable_set('ngplinks_cwp_volunteer_url', 'crmapi/volunteer');
  
  // Insert default user-defined node types into the database.
  $page = array(
    'type' => 'page',
    'name' => t('Other Page Types'),
    'module' => 'node',
    'description' => t('Create a page that does not fall into one of the preset types.  These pages do not allow comments.'),
    'custom' => TRUE,
    'modified' => TRUE,
    'locked' => FALSE,
  );

  $page = (object) _node_type_set_defaults($page);
  node_type_save($page);

  // Default content type options.
  variable_set('node_options_page', array('status', 'revision'));
  variable_set('comment_page', COMMENT_NODE_DISABLED);
  variable_set('node_options_news', array('status', 'promote', 'revision'));
  variable_set('comment_news', COMMENT_NODE_DISABLED);
  variable_set('node_options_press', array('status', 'promote', 'revision'));
  variable_set('comment_press', COMMENT_NODE_DISABLED);
  variable_set('node_options_event', array('status', 'promote', 'revision'));
  variable_set('comment_event', COMMENT_NODE_DISABLED);
  variable_set('node_options_action', array('status', 'promote', 'revision'));
  variable_set('comment_action', COMMENT_NODE_DISABLED);
  variable_set('node_options_video', array('status', 'promote', 'revision'));
  variable_set('comment_video', COMMENT_NODE_DISABLED);
  variable_set('node_options_issue', array('status', 'revision'));
  variable_set('comment_issue', COMMENT_NODE_DISABLED);
  
  // Add content creation links to managers toolbar
  $types = array(
    'action' => 1,
    'blog' => 1,
    'crmapi_node' => 1,
    'endorse' => 1,
    'event' => 1,
    'issue' => 1,
    'news' => 1,
    'page' => 1,
    'press' => 1,
    'video' => 1,
  );
  variable_set('manager_types', $types);
  
  // Default input format
  variable_set('filter_default_format', 2);
  
  // Features rebuild scripts.
  features_rebuild();
  
  // Create some initial content
  install_create_node('About', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.', array('type' => 'page', 'format' => 2));
  install_create_node('Sample News Article', 'Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, At accusam aliquyam diam diam dolore dolores duo eirmod eos erat, et nonumy sed tempor et et invidunt justo labore Stet clita ea et gubergren, kasd magna no rebum. sanctus sea sed takimata ut vero voluptua. est Lorem ipsum dolor sit amet.', array('type' => 'news', 'promote' => 1, 'format' => 2));
  install_create_node('Sample Press Release', 'Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', array('type' => 'press', 'promote' => 1, 'format' => 2));
  install_create_node('Sample Issue', 'Sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.', array('type' => 'issue', 'format' => 2));
  install_create_node('Sample Blog Post', 'Sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.', array('type' => 'blog', 'promote' => 1, 'format' => 2));
  
  $site_mail = variable_get('site_mail', 'info@' . str_replace('ngphost.', '', $_SERVER['SERVER_NAME']));
  $site_domain = str_replace('info@', '', $site_mail);
  $site_name = variable_get('site_name', '[Candidate for Office]');
  install_create_node('Privacy Policy', "<p>$site_name is committed to protecting your privacy online. While using our website, you do not have to identify yourself or divulge personal information. If you should choose to give us your personal information, you decide the amount of information you provide. </p>
<p>Our campaign is continually trying to expand online activism and to give voters the information they need in a timely manner. As a result, we may periodically ask you to provide information such as your name, address, phone number and email address in an effort to enhance our ability to work with you as an online activist. If you choose not to give us any information, we may not be able to provide you with timely information regarding events and issues important to you as a politically interested and active citizen. </p>
<p>$site_name maintain several interconnected websites at any given time. The principal website, $site_domain , is permanent. Other issue-specific websites may be created and removed as needed to highlight important issues and events. All websites owned and operated by $site_name are identified by the text \"Paid for by $site_name\"</p>
<p>Our privacy policy explains the information practices we use at $site_domain. </p>
<p>Our site may link to a limited number of other websites. $site_name is not responsible for the content or the privacy policies of these websites. </p>
<p>This privacy policy may be amended by $site_name at any time without notice other than posting of such amendments on this site.</p>
<p>$site_name will not sell your email address or other personal information to an outside, unaffiliated organization or any unauthorized third parties. </p>
<h3>When you contribute money online: </h3>
<p>When you contribute online, $site_name uses state-of-the-art security protocols through Verisign to protect sensitive data, such as personal information and credit card numbers. While this data is being transferred, it is protected by the Secure Sockets Layer (SSL) using a 128-bit signed certificate (the highest level commercially available). Before you even register or log in to Verisign, the Verisign server checks that you're using an approved browser - one that uses SSL 3.0 or higher. Once your information reaches Verisign, it resides on a server that is heavily guarded both physically and electronically. The Verisign servers sit behind an electronic firewall and are not directly connected to the Internet, so your private information is available only to authorized computers. </p>
<h3>When you sign up for one of our mailing lists: </h3>
<p>We collect your name, contact information, and email address, and certain other information. We use your email address to send you the email newsletter to which you subscribed. You may remove your email address from that subscriber list by visiting the unsubscribe page for that list. </p>
<h3>When you fill out one of our surveys: </h3>
<p>Your opinion matters to $site_name and we want your input. As a result, we may sometimes use surveys to help us understand which issues you are interested in. </p>
<h3>IP Addresses and Log File Data: </h3>
<p>We log IP address, which is the location of your computer or network on the Internet, for systems administration and troubleshooting purposes. We also use page hits in the aggregate to track the popularity of pages that people visit in order to improve the quality of the site. There is no personal identifiable information collected in our log files. </p>
<h3>Information on Children: </h3>
<p>Because we care about the safety and privacy of children online, we comply with the Children's Online Privacy Protection Act of 1998 (COPPA). COPPA and its accompanying FTC regulations establish United States federal law that protects the privacy of children using the Internet. We do not knowingly contact or collect personal information from children under 13. Our site is not intended to solicit information of any kind from children under 13, and we have designed our sites to block our knowing acceptance of information from children under 13 whenever age-related information is requested. </p>
<h3>Contacting us about privacy: </h3>
<p>If you have any questions about our privacy policy, the information we have collected from you online, the practices of this site or your interaction with this website, send email to $site_mail.</p>", array('type' => 'page', 'format' => 2));
  
  // Set up menu items for the about and privacy profile pages
  install_menu_create_menu_item('node/1', 'About', NULL, 'primary-links', NULL, -49);
  install_menu_create_menu_item('node/6', 'Privacy Policy', NULL, 'secondary-links', NULL, -50);
  
  // Set up path aliases for the about and privacy profile pages
  install_menu_create_url_alias('node/1', 'about');
  install_menu_create_url_alias('node/6', 'privacy');

  // Don't display date and author information for page nodes by default.
  $theme_settings = variable_get('theme_settings', array());
  $theme_settings['toggle_node_info_page'] = FALSE;
  $theme_settings['toggle_node_info_issue'] = FALSE;
  $theme_settings['toggle_node_info_event'] = FALSE;
  $theme_settings['toggle_node_info_crmapi_node'] = FALSE;
  variable_set('theme_settings', $theme_settings);

  // Set the default homepage
  variable_set('site_frontpage', 'home');
  
  // Configure LoginToboggan
  variable_set('logintoboggan_pre_auth_role', '2');
	variable_set('logintoboggan_purge_unvalidated_user_interval', '2592000');
	variable_set('logintoboggan_immediate_login_on_register', 1);
	variable_set('logintoboggan_override_destination_parameter', 1);
	variable_set('site_403', 'toboggan/denied');
	variable_set('logintoboggan_login_successful_message', '0');
	variable_set('logintoboggan_minimum_password_length', '8');
	
  // Configure roles and permissions
  install_add_role('administrator');
  install_add_user_to_role(1, install_get_rid('administrator'));
  install_add_permissions(install_get_rid('anonymous user'), array('access comments, access site-wide contact form, access epostcard, access forward, access letters, access ngplinks, access content, search content, view uploaded files'));
  install_add_permissions(install_get_rid('authenticated user'), array('access comments, post comments, post comments without approval, access site-wide contact form, access epostcard, access forward, access letters, access ngplinks, access content, search content, view uploaded files'));
  install_add_permissions(install_get_rid('administrator'), array('admin inline, admin menu, administer blocks, create blog entries, delete any blog entry, delete own blog entries, edit any blog entry, edit own blog entries, administer comments, administer site-wide contact form, administer CRM API, create CRM API forms, administer NGP API settings, administer features, create action content, create event content, create issue content, create news content, create press content, create video content, delete any action content, delete any event content, delete any issue content, delete any news content, delete any press content, delete any video content, delete own action content, delete own event content, delete own issue content, delete own news content, delete own press content, delete own video content, edit any action content, edit any event content, edit any issue content, edit any news content, edit any press content, edit any video content, edit own action content, edit own event content, edit own issue content, edit own news content, edit own press content, edit own video content, manage features, administer flickr, view all flickr photos, view own flickr photos, administer forward, override email address, override flood control, administer letters, administer manager interface, access managers bar, administer menu, access urchin, administer ngplinks, administer content types, administer nodes, create page content, delete any page content, delete own page content, delete revisions, edit any page content, edit own page content, revert revisions, view revisions, administer meta tags, edit meta tag DESCRIPTION, edit meta tag KEYWORDS, use page manager, administer pane visibility, use panels dashboard, view all panes, view pane admin links, administer url aliases, create url aliases, administer search, access administration pages, access site reports, administer actions, administer files, administer site configuration, select different theme, administer taxonomy, upload files, access user profiles, administer permissions, administer users, access all views, override node settings, override profile settings, allow reordering, administer follow, edit any user follow links, edit own follow links, edit site follow links, administer pathauto, notify of path changes, access petition stats, create petition, respond to petition'));

  // Set up CAPTCHA
  db_query("UPDATE {captcha_points} SET module = 'image_captcha', captcha_type = 'Image' WHERE form_id IN ('comment_form', 'contact_mail_page', 'user_register')");

  // Set up WYSIWYG
  db_query("INSERT INTO {wysiwyg} (format, editor, settings) VALUES (1, '', NULL), (2, 'tinymce', '%s')", 'a:20:{s:7:"default";i:1;s:11:"user_choose";i:0;s:11:"show_toggle";i:1;s:5:"theme";s:8:"advanced";s:8:"language";s:2:"en";s:7:"buttons";a:6:{s:7:"default";a:17:{s:4:"bold";i:1;s:6:"italic";i:1;s:9:"underline";i:1;s:11:"justifyleft";i:1;s:13:"justifycenter";i:1;s:12:"justifyright";i:1;s:7:"bullist";i:1;s:7:"numlist";i:1;s:7:"outdent";i:1;s:6:"indent";i:1;s:4:"undo";i:1;s:4:"redo";i:1;s:4:"link";i:1;s:6:"unlink";i:1;s:5:"image";i:1;s:9:"forecolor";i:1;s:10:"blockquote";i:1;}s:8:"advimage";a:1:{s:8:"advimage";i:1;}s:5:"paste";a:1:{s:9:"pasteword";i:1;}s:5:"media";a:1:{s:5:"media";i:1;}s:4:"imce";a:1:{s:4:"imce";i:1;}s:6:"drupal";a:1:{s:5:"break";i:1;}}s:11:"toolbar_loc";s:3:"top";s:13:"toolbar_align";s:4:"left";s:8:"path_loc";s:6:"bottom";s:8:"resizing";i:1;s:11:"verify_html";i:1;s:12:"preformatted";i:0;s:22:"convert_fonts_to_spans";i:1;s:17:"remove_linebreaks";i:1;s:23:"apply_source_formatting";i:0;s:27:"paste_auto_cleanup_on_paste";i:0;s:13:"block_formats";s:32:"p,address,pre,h2,h3,h4,h5,h6,div";s:11:"css_setting";s:4:"self";s:8:"css_path";s:40:"/sites/all/themes/blueprint/css/tiny.css";s:11:"css_classes";s:162:"Font Size 10=font-size-10 Font Size 12=font-size-12 Font Size 14=font-size-14 Font Size 16=font-size-16 Font Size 18=font-size-18 Font Size 20=font-size-20 ";}');
  
  variable_set('imce_roles_profiles', array(3 => array("pid" => "1"), 2 => array("pid" => "0"), 1 => array("pid" => "0")));
  
  // Set up custom date formats
  date_format_save(array('format' => '<\s\t\ro\n\g>M</\s\t\ro\n\g><\e\m>j</\e\m>', 'type' => 'custom', 'locked' => 0, 'is_new' => 1));
  date_format_type_save(array('title' => 'Tiny', 'type' => 'tiny', 'locked' => 0, 'is_new' => 1));
  variable_set('date_format_tiny', '<\s\t\ro\n\g>M</\s\t\ro\n\g><\e\m>j</\e\m>');
  
  // Admin toolbar settings
  variable_set('admin_toolbar', array(
    "layout" => "horizontal",
    "position" => "sw",
    "behavior" => "df",
    "blocks" => array(
      "spaces-menu_editor" => -1,
      "context_ui-editor" => -1,
      "context_ui-devel" => -1,
      "admin-create" => -1,
      "admin-theme" => 1,
      "admin-account" => -1,
      "admin-menu" => 1,
      "admin-devel" => -1,
    ),
  ));
}

/**
 * Configuration. Third stage.
 */
function _ngpprofile_campaign_configure_check() {
  // Rebuild key tables/caches
  module_rebuild_cache(); // Detects the newly added bootstrap modules
  node_access_rebuild();
  drupal_get_schema(NULL, TRUE); // Clear schema DB cache
  drupal_flush_all_caches();
  views_invalidate_cache(); // Rebuild the views.
  // This one is done by the installer alone
  // menu_rebuild();       // Rebuild the menu.
  
  // Enable the right theme. This must be handled after drupal_flush_all_caches()
  // which rebuilds the system table based on a stale static cache,
  // blowing away our changes.
  _ngpprofile_system_theme_data();
  install_disable_theme('garland');
  install_enable_theme('ngp');
  install_enable_theme('clean');
  install_enable_theme('custom');
  install_default_theme('glass');
  
  // Enable NGP blocks
  // We have to manually set the block appearance for each theme since these
  // values do not appear to get inherited by default.
  //
  // NGP Base theme settings
  install_add_block('crmngp', '0', 'ngp', 1, -50, 'right', 1, NULL, NULL, NULL, 'Take Action');
  install_add_block('menu', 'action', 'ngp', 1, -49, 'right', 1, NULL, NULL, NULL, '<none>');
  // NGP Clean settings
  install_add_block('crmngp', '0', 'clean', 1, -50, 'right', 1, NULL, NULL, NULL, 'Take Action');
  install_add_block('menu', 'action', 'clean', 1, -49, 'right', 1, NULL, NULL, NULL, '<none>');
  // NGP Custom settings
  install_add_block('crmngp', '0', 'custom', 1, -50, 'right', 1, NULL, NULL, NULL, 'Take Action');
  install_add_block('menu', 'action', 'custom', 1, -49, 'right', 1, NULL, NULL, NULL, '<none>');
  // NGP Glass settings
  install_add_block('crmngp', '0', 'glass', 1, -50, 'right', 1, NULL, NULL, NULL, 'Take Action');
  install_add_block('menu', 'action', 'glass', 1, -49, 'right', 1, NULL, NULL, NULL, '<none>');
  
  // Set admin theme to Rubik
  variable_set('admin_theme', 'rubik');
  variable_set('node_admin_theme', 1);
  
  // Disable user blocks
  //
  // NGP Base theme settings
  install_disable_block('user', '0', 'ngp');
  install_disable_block('user', '1', 'ngp');
  install_disable_block('system', '0', 'ngp');
  // NGP Clean settings
  install_disable_block('user', '0', 'clean');
  install_disable_block('user', '1', 'clean');
  install_disable_block('system', '0', 'clean');
  // NGP Custom settings
  install_disable_block('user', '0', 'custom');
  install_disable_block('user', '1', 'custom');
  install_disable_block('system', '0', 'custom');
  // NGP Glass settings
  install_disable_block('user', '0', 'glass');
  install_disable_block('user', '1', 'glass');
  install_disable_block('system', '0', 'glass');
  
  // Rubik settings
  install_disable_block('crmngp', '0', 'rubik');
  install_disable_block('menu', 'action', 'rubik');
  
  node_access_rebuild();
}

/**
 * Finish configuration batch
 */
function _ngpprofile_campaign_configure_finished($success, $results) {
  variable_set('ngpprofile_install', 1);
  // Get out of this batch and let the installer continue.
  variable_set('install_task', 'profile-finished');
}

/**
 * Reimplementation of system_theme_data(). The core function's static cache
 * is populated during install prior to active install profile awareness.
 * This workaround makes enabling themes in profiles/ngpprofile/themes possible.
 */
function _ngpprofile_system_theme_data() {
  global $profile;
  $profile = 'ngpprofile';

  $themes = drupal_system_listing('\.info$', 'themes');
  $engines = drupal_system_listing('\.engine$', 'themes/engines');

  $defaults = system_theme_default();

  $sub_themes = array();
  foreach ($themes as $key => $theme) {
    $themes[$key]->info = drupal_parse_info_file($theme->filename) + $defaults;

    if (!empty($themes[$key]->info['base theme'])) {
      $sub_themes[] = $key;
    }

    $engine = $themes[$key]->info['engine'];
    if (isset($engines[$engine])) {
      $themes[$key]->owner = $engines[$engine]->filename;
      $themes[$key]->prefix = $engines[$engine]->name;
      $themes[$key]->template = TRUE;
    }

    // Give the stylesheets proper path information.
    $pathed_stylesheets = array();
    foreach ($themes[$key]->info['stylesheets'] as $media => $stylesheets) {
      foreach ($stylesheets as $stylesheet) {
        $pathed_stylesheets[$media][$stylesheet] = dirname($themes[$key]->filename) .'/'. $stylesheet;
      }
    }
    $themes[$key]->info['stylesheets'] = $pathed_stylesheets;

    // Give the scripts proper path information.
    $scripts = array();
    foreach ($themes[$key]->info['scripts'] as $script) {
      $scripts[$script] = dirname($themes[$key]->filename) .'/'. $script;
    }
    $themes[$key]->info['scripts'] = $scripts;

    // Give the screenshot proper path information.
    if (!empty($themes[$key]->info['screenshot'])) {
      $themes[$key]->info['screenshot'] = dirname($themes[$key]->filename) .'/'. $themes[$key]->info['screenshot'];
    }
  }

  foreach ($sub_themes as $key) {
    $themes[$key]->base_themes = system_find_base_themes($themes, $key);
    // Don't proceed if there was a problem with the root base theme.
    if (!current($themes[$key]->base_themes)) {
      continue;
    }
    $base_key = key($themes[$key]->base_themes);
    foreach (array_keys($themes[$key]->base_themes) as $base_theme) {
      $themes[$base_theme]->sub_themes[$key] = $themes[$key]->info['name'];
    }
    // Copy the 'owner' and 'engine' over if the top level theme uses a
    // theme engine.
    if (isset($themes[$base_key]->owner)) {
      if (isset($themes[$base_key]->info['engine'])) {
        $themes[$key]->info['engine'] = $themes[$base_key]->info['engine'];
        $themes[$key]->owner = $themes[$base_key]->owner;
        $themes[$key]->prefix = $themes[$base_key]->prefix;
      }
      else {
        $themes[$key]->prefix = $key;
      }
    }
  }

  // Extract current files from database.
  system_get_files_database($themes, 'theme');
  db_query("DELETE FROM {system} WHERE type = 'theme'");
  foreach ($themes as $theme) {
    $theme->owner = !isset($theme->owner) ? '' : $theme->owner;
    db_query("INSERT INTO {system} (name, owner, info, type, filename, status, throttle, bootstrap) VALUES ('%s', '%s', '%s', '%s', '%s', %d, %d, %d)", $theme->name, $theme->owner, serialize($theme->info), 'theme', $theme->filename, isset($theme->status) ? $theme->status : 0, 0, 0);
  }
}


/**
 * Alter some forms implementing hooks in system module namespace
 * 
 * This is a trick for hooks to get called, otherwise we cannot alter forms
 */

// Set NGP as default profile
function system_form_install_select_profile_form_alter(&$form, $form_state) {
  foreach($form['profile'] as $key => $element) {
    $form['profile'][$key]['#value'] = 'ngpprofile';
  }
}

/**
 * Set English as default language.
 * 
 * If no language selected, the installation crashes. I guess English should be the default 
 * but it isn't in the default install. @todo research, core bug?
 */
function system_form_install_select_locale_form_alter(&$form, $form_state) {
  $form['locale']['en']['#value'] = 'en';
}

/**
 * Alter the install profile configuration form and provide timezone location options.
 */
function system_form_install_configure_form_alter(&$form, $form_state) {
  if (function_exists('date_timezone_names') && function_exists('date_timezone_update_site')) {
    $form['server_settings']['date_default_timezone']['#access'] = FALSE;
    $form['server_settings']['#element_validate'] = array('date_timezone_update_site');
    $form['server_settings']['date_default_timezone_name'] = array(
      '#type' => 'select',
      '#title' => t('Default time zone'),
      '#default_value' => NULL,
      '#options' => date_timezone_names(FALSE, TRUE),
      '#description' => t('Select the default site time zone. If in doubt, choose the timezone that is closest to your location which has the same rules for daylight saving time.'),
      '#required' => TRUE,
    );
  }
}